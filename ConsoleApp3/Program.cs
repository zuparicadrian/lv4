﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset FirstTest = new Dataset("test.csv");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] result = adapter.CalculateAveragePerColumn(FirstTest);
            foreach (double print in result)
            {
                Console.WriteLine(print);
            }

            List<IRentable> rent = new List<IRentable>();
            Video SecondTest = new Video("The Godfather");
            Book ThirdTest = new Book("The Fall of Gondolin");

            rent.Add(SecondTest);
            rent.Add(ThirdTest);

            RentingConsolePrinter print1 = new RentingConsolePrinter();

            print1.DisplayItems(rent);
            print1.PrintTotalPrice(rent);
            
        }
    }
}
